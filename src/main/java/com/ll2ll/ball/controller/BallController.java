package com.ll2ll.ball.controller;

import com.ll2ll.ball.dao.BallMapper;
import com.ll2ll.ball.service.BallService;
import com.ll2ll.ball.service.DaLeTouService;
import com.ll2ll.ball.utils.HttpClientUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BallController {

    @Autowired
    private BallService ballService;
    @Autowired
    private BallMapper ballMapper;

    @Autowired
    DaLeTouService daLeTouService;
    @RequestMapping("/ball")
    public Object getBalls(){
        ballService.getBalls();
        String latestDate = ballMapper.findLatestDate();
        return latestDate;
    }

    @RequestMapping("/dlt")
    public void addDltBalls(String pageSize,String pageNum){
        daLeTouService.addBalls(pageSize,pageNum);

    }
    @RequestMapping("/list")
    public Object getDltBalls(@RequestParam int count){
        return daLeTouService.getNumberList(count);
    }
}
