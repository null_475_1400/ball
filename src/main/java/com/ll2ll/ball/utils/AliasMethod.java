package com.ll2ll.ball.utils;

import java.util.*;

public class AliasMethod {
        /* The probability and alias tables. */
        private int[] _alias;
        private double[] _probability;

        public AliasMethod(List<Double> probabilities) {

            /* Allocate space for the probability and alias tables. */
            _probability = new double[probabilities.size()];
            _alias = new int[probabilities.size()];

            /* Compute the average probability and cache it for later use. */
            double average = 1.0 / probabilities.size();

            /* Create two stacks to act as worklists as we populate the tables. */
            Stack<Integer> small = new Stack<>();
            Stack<Integer> large = new Stack<>();

            /* Populate the stacks with the input probabilities. */
            for (int i = 0; i < probabilities.size(); ++i) {
                /* If the probability is below the average probability, then we add
                 * it to the small list; otherwise we add it to the large list.
                 */
                if (probabilities.get(i) >= average)
                    large.push(i);
                else
                    small.push(i);
            }

            /* As a note: in the mathematical specification of the algorithm, we
             * will always exhaust the small list before the big list.  However,
             * due to floating point inaccuracies, this is not necessarily true.
             * Consequently, this inner loop (which tries to pair small and large
             * elements) will have to check that both lists aren't empty.
             */
            while (small.size() > 0 && large.size() > 0) {
                /* Get the index of the small and the large probabilities. */
                int less = small.pop();
                int more = large.pop();

                /* These probabilities have not yet been scaled up to be such that
                 * 1/n is given weight 1.0.  We do this here instead.
                 */
                _probability[less] = probabilities.get(less) * probabilities.size();
                _alias[less] = more;

                /* Decrease the probability of the larger one by the appropriate
                 * amount.
                 */
                probabilities.set(more,(probabilities.get(more) + probabilities.get(less) - average));

                /* If the new probability is less than the average, add it into the
                 * small list; otherwise add it to the large list.
                 */
                if (probabilities.get(more) >= average)
                    large.push(more);
                else
                    small.push(more);
            }

            /* At this point, everything is in one list, which means that the
             * remaining probabilities should all be 1/n.  Based on this, set them
             * appropriately.  Due to numerical issues, we can't be sure which
             * stack will hold the entries, so we empty both.
             */
            while (small.size() >0 )
                _probability[small.pop()] = 1.0;
            while (large.size() > 0)
                _probability[large.pop()] = 1.0;
        }

        /**
         * Samples a value from the underlying distribution.
         *
         * @return A random value sampled from the underlying distribution.
         */
        public int next() {

            long tick = System.currentTimeMillis();
            int seed = ((int)(tick & 0xffffffffL) | (int)(tick >> 32 ));

            seed = (seed + UUID.randomUUID().hashCode() + new Random().nextInt(100));

            Random random = new Random(seed);
            int column = random.nextInt(_probability.length);

            /* Generate a biased coin toss to determine which option to pick. */
            boolean coinToss = random.nextDouble() < _probability[column];

            return coinToss ? column : _alias[column];
        }

}
