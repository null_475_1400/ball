package com.ll2ll.ball.service;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ll2ll.ball.dao.BallMapper;
import com.ll2ll.ball.dao.DaLeTouBallMapper;
import com.ll2ll.ball.model.Ball;
import com.ll2ll.ball.utils.AliasMethod;
import com.ll2ll.ball.utils.HttpClientUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DaLeTouService {

    @Value("${daLeTouUrl}")
    private String daLeTouUrl;

    @Autowired
    DaLeTouBallMapper ballMapper;

    public void addBalls(String pageSize,String pageNum){
        String resultBall = HttpClientUtils.doHttpGet(daLeTouUrl.replace("PAGE_SIZE",pageSize).replace("PAGE_NUM",pageNum));

        Map<String,Object> ballMap = new Gson().fromJson(resultBall,new TypeToken<Map<String,Object>>(){}.getType());
        Map<String,Object> listMap = new Gson().fromJson(new Gson().toJson(ballMap.get("value")),new TypeToken<Map<String,Object>>(){}.getType());
        List<Map<String,Object>> ballList = new Gson().fromJson(new Gson().toJson(listMap.get("list")),new TypeToken<List<Map<String,Object>>>(){}.getType());
        ballList.forEach(b->{
            Ball ball = new Ball();
            ball.setId(b.get("lotteryDrawNum").toString());
            ball.setRedBall(b.get("lotteryDrawResult").toString().substring(0,14).replace(" ","-"));
            ball.setBlueBall(b.get("lotteryDrawResult").toString().substring(15,20).replace(" ","-"));
//            System.out.println(ball);
            ballMapper.insert(ball);

        });

    }

    public List<String> getNumberList(int count) {
        List<String> resultList = new ArrayList<>();
        List<Map<String,Object>> redList = ballMapper.findRedBallPercent();
        List<Map<String,Object>> blueList = ballMapper.findBlueBallPercent();

        Map<String,Double> redMap = new HashMap<>();
        Map<String,Double> blueMap = new HashMap<>();
        redList.forEach(r-> redMap.put(String.valueOf(r.get("ball")),Double.valueOf(String.valueOf(r.get("countPer")))));
        blueList.forEach(r-> blueMap.put(String.valueOf(r.get("ball")),Double.valueOf(String.valueOf(r.get("countPer")))));

        for (int j=0;j<count;j++){


            List<Double> redlist = new ArrayList<>(redMap.values());
            List<Double> bluelist = new ArrayList<>(blueMap.values());
            List<String> redgifts = new ArrayList<>(redMap.keySet());
            List<String> bluegifts = new ArrayList<>(blueMap.keySet());

            AliasMethod methodRed = new AliasMethod(redlist);
            AliasMethod methodBlue = new AliasMethod(bluelist);



            List<String> redTempList = new ArrayList<>();
            List<String> blueTempList = new ArrayList<>();
            while (redTempList.size()<5) {
                int index = methodRed.next();
                String key = redgifts.get(index);
                if (!redTempList.contains(key)){
                    redTempList.add(key);
                }
            }
            while (blueTempList.size()<2) {
                int index = methodBlue.next();
                String key = bluegifts.get(index);
                if (!blueTempList.contains(key)){
                    blueTempList.add(key);
                }
            }
            String  redTemp = redTempList.stream().sorted().collect(Collectors.joining(" "));
            String blueTemp = blueTempList.stream().sorted().collect(Collectors.joining(" "));

            resultList.add(redTemp+" - "+blueTemp);
            System.out.println(resultList);
        }
        return resultList;
    }
}
