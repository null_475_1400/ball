package com.ll2ll.ball.service;

import com.ll2ll.ball.dao.BallMapper;
import com.ll2ll.ball.model.Ball;
import com.ll2ll.ball.utils.HttpClientUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class BallService {

    @Autowired
    private BallMapper ballMapper;

    public String getBalls() {
        String result = HttpClientUtils.doHttpGet("http://kaijiang.500.com/ssq.shtml");

        Document document = Jsoup.parse(result);

        Elements elements = document.select("a#change_date");

        Element element = elements.get(0);
        String changeDate = element.text();

        String latestDate = ballMapper.findLatestDate();
        if (StringUtils.isBlank(latestDate)) {
            latestDate = "3001";
        }

        for (int i = Integer.parseInt(changeDate); i >= Integer.parseInt(latestDate); i--) {
            try {
                TimeUnit.SECONDS.sleep(2);

                String dateHtml = String.valueOf(i);
                if (i <= 9154) {
                    dateHtml = "0" + i;
                }
                System.out.println(dateHtml);
                Ball ball1Find = ballMapper.findOne(dateHtml);
                if (ball1Find != null) {
                   continue;
                }
                String resultBall = HttpClientUtils.doHttpGet("http://kaijiang.500.com/shtml/ssq/" + dateHtml + ".shtml");

                Document documentBall = Jsoup.parse(resultBall);

                Elements elementsRedBall = documentBall.select("div.ball_box01 >ul>.ball_red");
                Elements elementsBlueBall = documentBall.select("div.ball_box01>ul>.ball_blue");

                String blueBall = elementsBlueBall.get(0).text();
                List<String> redBallList = new ArrayList<>();
                elementsRedBall.forEach(e -> {
                    String ball = e.text();
                    redBallList.add(ball);
                });

                redBallList.sort(Comparator.naturalOrder());
                String redBall = StringUtils.join(redBallList.toArray(), "-");

                Ball ball = new Ball();
                ball.setId(dateHtml);
                ball.setRedBall(redBall);
                ball.setBlueBall(blueBall);
                ballMapper.insert(ball);

            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
        return result;
    }

    public String getBestBalls(){

        List<Ball> ballList = ballMapper.findAll();


        return "";
    }
}
