package com.ll2ll.ball.model;

public class Ball {

    private String id;

    private String redBall;

    private String blueBall;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRedBall() {
        return redBall;
    }

    public void setRedBall(String redBall) {
        this.redBall = redBall;
    }

    public String getBlueBall() {
        return blueBall;
    }

    public void setBlueBall(String blueBall) {
        this.blueBall = blueBall;
    }

    @Override
    public String toString() {
        return "Ball{" +
                "id='" + id + '\'' +
                ", redBall='" + redBall + '\'' +
                ", blueBall='" + blueBall + '\'' +
                '}';
    }
}
