package com.ll2ll.ball;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = "com.ll2ll.ball.*")
public class BallApplication {

    public static void main(String[] args) {
        SpringApplication.run(BallApplication.class, args);
    }

}
