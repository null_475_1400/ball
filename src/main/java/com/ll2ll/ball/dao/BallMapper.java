package com.ll2ll.ball.dao;

import com.ll2ll.ball.model.Ball;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BallMapper {

    List<Ball> findList(Ball ball);

    int insert(Ball ball);

    Ball findOne(String id);

    List<Ball> findAll();

    String findLatestDate();
}
