import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Map;


/**
 * Ognl工具类
 * <p>
 * 主要是为了在ognl表达式访问静态方法时可以减少长长的类名称编写 Ognl访问静态方法的表达式
 * </p>
 * 
 * 示例使用:
 * 
 * <pre>
 * 	&lt;if test=&quot;@Ognl@isNotEmpty(userId)&quot;&gt;
 * 	and user_id = #{userId}
 * &lt;/if&gt;
 * </pre>
 * 
 * @author lujun
 */
public class Ognl {

	/**
	 * 使用ognl扩展 该功能为，根据传入的值， 如果值为0，则 ... 。 如果值为1，则 ... 。
	 * 
	 * @return boolean
	 */
	public static boolean isSolve(Object o, String soleState) {
		if (o == null) {
			return false;
		}
		String str = null;
		if (o instanceof String[]) {
			String[] objects = (String[]) o;
			str = objects[0];
		} else if (o instanceof Character) {
			Character c = (Character) o;
			str = Character.toString(c);
		} else if (o instanceof String) {
			String s = (String) o;
			str = s;
		}
		if (soleState.equals(str)) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 可以用于判断 Map,Collection,String,Array是否为空
	 * 
	 * @param o
	 * @return boolean
	 */
	@SuppressWarnings({ "rawtypes", "unused" })
	public static boolean isEmpty(Object o) throws IllegalArgumentException {
		if (o == null)
			return true;
		if (o instanceof String) {
			if (((String) o).trim().length() == 0) {
				return true;
			}
		} else if (o instanceof Collection) {
			if (((Collection) o).isEmpty()) {
				return true;
			}
		} else if (o.getClass().isArray()) {
			if (((Object[]) o).length == 0) {
				return true;
			}
		} else if (o instanceof Map) {
			if (((Map) o).isEmpty()) {
				return true;
			}
		} else if (o instanceof Long) {
			if (((Long) o) == null) {
				return true;
			}
		} else if (o instanceof Short) {
			if (((Short) o) == null) {
				return true;
			}
		} else {
			return false;
		}
		return false;
	}

	/**
	 * 可以用于判断 Map,Collection,String,Array是否不为空
	 * 
	 * @param o
	 * @return boolean
	 */
	public static boolean isNotEmpty(Object o) {
		return !isEmpty(o);
	}

	public static boolean isNotEmpty(Object... objects) {
		if (objects == null || objects.length == 0)
			return false;
		for (Object obj : objects) {
			if (isEmpty(obj)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNotBlank(Object o) {
		return !isBlank(o);
	}

	public static boolean isBlank(Object o) {
		return StringUtils.isBlank((String) o);
	}

	public static boolean isBlank(String str) {
		return StringUtils.isBlank(str);
	}
}
